package com.uca.dao;

import java.util.List;
import java.util.ArrayList;

import com.uca.entity.*;

import java.sql.*;

public class GridDAO{
    Connection connection = _Connector.getMainConnection();
    
    public List<CellEntity> getCellsFromDB(Connection c){
	GridEntity grid = new GridEntity();

        try{
            PreparedStatement statement = c.prepareStatement("SELECT * FROM plateau WHERE etat=true;");
            ResultSet resultSet = statement.executeQuery();
            CellEntity cell;
            while(resultSet.next()){
                cell = new CellEntity(resultSet.getInt("x"), resultSet.getInt("y"));
                grid.addCell(cell);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
	return grid.getCells();
    }

    public void changeState(Connection c, CellEntity cell){
	try{
            PreparedStatement statement;
	    ResultSet resultSet;
	    statement = c.prepareStatement("SELECT etat FROM plateau WHERE x=? and y=?;");
	    statement.setInt(1,cell.getX());
	    statement.setInt(2,cell.getY());
	    resultSet = statement.executeQuery();
	    
	    statement = c.prepareStatement("BEGIN;UPDATE plateau SET etat=? WHERE x=? and y=?;");
	    if(resultSet.next()){
		statement.setBoolean(1,!resultSet.getBoolean("etat"));
		statement.setInt(2,cell.getX());
		statement.setInt(3,cell.getY());
		statement.executeUpdate();
	    }	    
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void emptyGrid(Connection c){
	try{
            PreparedStatement statement = c.prepareStatement("BEGIN;UPDATE plateau SET etat=false WHERE etat=true;");
	    statement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void save(Connection c){
	try{
	    c.commit();
	}
	catch(SQLException e){
	    e.printStackTrace();
	}
    }

    public void cancel(Connection c){
	try{
	    c.rollback();
	}
	catch(SQLException e){
	    e.printStackTrace();
	}
    }

    public void newGeneration(Connection c){
	try{
	    List<CellEntity> cells = this.getCellsFromDB(c);

	    // on récupère les corronnées maximales du carré contenant
	    // toutes les cellules vivantes
	    if(cells.size() > 0){
		int x_min = cells.get(0).getX(), x_max = cells.get(0).getX();
		int y_min = cells.get(0).getY(), y_max = cells.get(0).getY();

		for(CellEntity cell : cells){
		    int cell_x = cell.getX();
		    if(cell_x < x_min){
			x_min = cell_x;
		    }
		    if(cell_x > x_max){
			x_max = cell_x;
		    }

		    int cell_y = cell.getY();
		    if(cell_y < y_min){
			y_min = cell_y;
		    }
		    if(cell_y > y_max){
			y_max = cell_y;
		    }
		}
		
		PreparedStatement statement = c.prepareStatement("BEGIN;UPDATE plateau SET etat = NOT etat "+
								 "WHERE (x, y) IN (SELECT subquery.x, subquery.y FROM "+
								 "(SELECT p.x, p.y, p.etat, COUNT(v.*) AS nombre_voisins_vivants "+
								 "FROM plateau p LEFT JOIN plateau v ON ( "+
								 "v.x >= p.x - 1 AND v.x <= p.x + 1 AND "+
								 "v.y >= p.y - 1 AND v.y <= p.y + 1 AND "+
								 "v.etat = true AND "+
								 "(v.x, v.y) <> (p.x, p.y)) "+
								 "WHERE p.x >= ? AND p.x <= ? AND p.y >= ? AND p.y <= ? "+
								 "GROUP BY p.x, p.y) AS subquery "+
								 "WHERE (etat = true AND nombre_voisins_vivants NOT IN (2, 3)) OR (etat = false AND nombre_voisins_vivants = 3));");
		x_min--;
		x_max++;
		y_min--;
		y_max++;
		statement.setInt(1,x_min);
		statement.setInt(2,x_max);
		statement.setInt(3,y_min);
		statement.setInt(4,y_max);
		statement.executeUpdate();
	    }
	}
	catch(SQLException e){
	    e.printStackTrace();
	}
    }

    public boolean contains(List<CellEntity> cells, CellEntity c){
	for(CellEntity cell : cells){
	    if(cell.getX() == c.getX() && cell.getY() == c.getY()){
		return true;
	    }
	}
	return false;
    }
}
