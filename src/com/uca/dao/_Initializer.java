package com.uca.dao;

import java.sql.*;

public class _Initializer {
    // nom de la table contenant la grille
    final static String TABLE = "plateau";
    // taille de grille
    final static int SIZE = 100;

    /**
     * cette méthode permet d'initialiser en créant une table pour la grille si elle n'existe pas
     */
    public static void Init(){
        Connection connection = _Connector.getMainConnection();
        try{
            PreparedStatement statement;// = connection.prepareStatement("DROP TABLE plateau;");
            //statement.executeUpdate();
	    
            if(! tableExists(connection, _Initializer.TABLE)){
                statement = connection.prepareStatement("CREATE TABLE plateau (x int, y int, etat boolean, primary key(x,y));");
		statement.executeUpdate();
		PreparedStatement statement2;	    
		for(int i = 0; i<SIZE;i++){
		    for(int j = 0; j<SIZE; j++){
			statement2 = connection.prepareStatement("BEGIN;INSERT INTO plateau (x, y, etat) VALUES(?,?,?);END;");
			statement2.setInt(1, i);
			statement2.setInt(2, j);
			statement2.setBoolean(3, false);
			statement2.executeUpdate();
		    }
		}
		connection.commit();
            }
        } catch (Exception e){
            System.out.println(e.toString());
            throw new RuntimeException("could not create database !");
        }
    }

    /**
     * teste si une table existe dans la base de données 
     */
    private static boolean tableExists(Connection connection, String tableName) throws SQLException {
        DatabaseMetaData meta = connection.getMetaData();
        ResultSet resultSet = meta.getTables(null, null, tableName, new String[]{"TABLE"});
        return resultSet.next();
    }
}
