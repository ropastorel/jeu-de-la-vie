package com.uca.dao;

import java.util.*;
import java.sql.*;

import java.util.HashMap;

public class _Connector {

    private static String url = "jdbc:postgresql://localhost/life";
    private static String user = "nemesis";
    private static String passwd = "admin";

    private static Connection connect;

    public static Connection getMainConnection(){
        if(connect == null){
            connect = getNewConnection();
        }
        return connect;
    }

    private static Connection getNewConnection() {
        Connection c;
        try {
            c = DriverManager.getConnection(url, user, passwd);
	    c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    c.setAutoCommit(false);
        } catch (SQLException e) {
            System.err.println("Erreur en ouvrant une nouvelle connection.");
            throw new RuntimeException(e);
        }
        return c;
    }

    public static Connection getConnection(int id, HashMap<Integer, Connection> allConnections){
	Connection c = allConnections.get(id);
	if(c == null){
	    c = getNewConnection();
	    allConnections.put(id,c);
	}
	return c;
    }
}
